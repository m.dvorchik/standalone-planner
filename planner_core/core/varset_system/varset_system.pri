HEADERS += \
    $$PWD/UState.h \
    $$PWD/UTransition.h \
    $$PWD/boolvar_system.h \
    $$PWD/combinedvar_system.h \
    $$PWD/floatvar_system.h \
    $$PWD/multivar_system.h \
    $$PWD/transition_index.h \
    $$PWD/varset_system_base.h

SOURCES += \
    $$PWD/UState.cpp \
    $$PWD/UTransition.cpp \
    $$PWD/UTransitionSystem.cpp
