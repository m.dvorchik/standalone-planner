include(algorithm/algorithm.pri)
include(complex_hashset/complex_hashset.pri)
include(containers/containers.pri)
include(io/io.pri)
include(search_algorithms/search_algorithms.pri)
include(utils/utils.pri)
include(varset_system/varset_system.pri)

HEADERS += \
    $$PWD/UError.h \
    $$PWD/UExternalMemoryController.h \
    $$PWD/UMatrix.h \
    $$PWD/USSingleton.h \
    $$PWD/avl_tree.h \
    $$PWD/bit_container.h \
    $$PWD/byte_range.h \
    $$PWD/compressed_stream.h \
    $$PWD/config.h \
    $$PWD/delayed_buffer.h \
    $$PWD/hash.h \
    $$PWD/masked_bit_vector.h \
    $$PWD/numeric_expression.h \
    $$PWD/problem_instance_generator.h \
    $$PWD/search_database.h \
    $$PWD/search_queue.h \
    $$PWD/state_space_solver.h \
    $$PWD/thread_pool.h \
    $$PWD/transition_system.h

SOURCES += \
    $$PWD/UError.cpp \
    $$PWD/UExternalMemoryController.cpp \
    $$PWD/UMatrix.cpp \
    $$PWD/bit_container.cpp \
    $$PWD/masked_bit_vector.cpp \
    $$PWD/numeric_expression.cpp
