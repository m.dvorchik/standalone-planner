HEADERS += \
    $$PWD/cache.h \
    $$PWD/cached_file.h \
    $$PWD/data_file.h \
    $$PWD/data_file_unix.h \
    $$PWD/data_file_win.h \
    $$PWD/streamer.h

SOURCES += \
    $$PWD/data_file.cpp
