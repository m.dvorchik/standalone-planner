HEADERS += \
    $$PWD/astar_engine.h \
    $$PWD/batched_engine.h \
    $$PWD/bfs_engine.h \
    $$PWD/dfs_engine.h \
    $$PWD/greedy_bfs_engine.h \
    $$PWD/queued_engine.h \
    $$PWD/search_engine_base.h \
    $$PWD/serial_engine.h
